import { Component, OnInit } from '@angular/core';
import { timeout } from 'q';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {

  allowServerProcess = true;
  serverStatus = '';
  serverState = 'Hello Binding.';
  serverCreated = false;
  serverList = ['server1', 'server2'];
  constructor() {
    setTimeout(() => {
      this.allowServerProcess = false;
    }, 2000);
   }

  ngOnInit() {
  }

  onServerCreate() {
    this.serverCreated = true;
    this.serverStatus = 'Server is created!';
    this.serverList.push(this.serverState);
  }

  UpdateServer(event: any) {
    this.serverState = event.target.value;
  }
}
