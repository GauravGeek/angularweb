import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-assignment3',
  templateUrl: './assignment3.component.html',
  styleUrls: ['./assignment3.component.css']
})
export class Assignment3Component implements OnInit {

  isBtnClicked = false;
  secretPwd = '****';
  btnClickTimeStamp: any = 0;
  BtnClickLogList = [];
  count = 0;
  constructor() { }

  ngOnInit() {
  }

  DisplayBtn_Click(event: any) {
    this.isBtnClicked = !this.isBtnClicked;
    this.secretPwd = !this.isBtnClicked ? '****' : 'tuna';
    this.BtnClickLogList.push(++this.count);
  }

}
